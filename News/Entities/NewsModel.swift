//
//  NewsModel.swift
//  News
//
//  Created by MAC12166 on 6/14/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
class NewsModel{
    var title: String = ""
    var content1: String = ""
    var content2: String = ""
    var imageUrl: String = ""
    var author: String = ""
    var time: Date = Date()
    
    init(title: String, content1: String, content2: String, imageUrl: String, author: String, time: Date) {
        self.title = title
        self.content1 = content1
        self.content2 = content2
        self.imageUrl = imageUrl
        self.author = author
        self.time = time  
    }
    
    init(){}
    
    func parseTime() -> String{
        let currentDate = Date()
        let calendar = Calendar.current
        let intervalTime = currentDate.timeIntervalSince(self.time)
        
        if calendar.isDateInToday(time) {
            
            let hour = Int(intervalTime/3600)
            if hour > 1 {
                return "\(hour) hour"
            } else{
                return "Just now"
            }
            
        } else if intervalTime >= 172800 {
            return "Old"
        } else{
            return "Yesterday"
        }
    }
}
