//
//  NewsDetailWireFrameProtocol.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
import UIKit

protocol NewsDetailWireFrameProtocol {
    static func createNewsDetailModule(forNews news: NewsModel) -> UIViewController
}
