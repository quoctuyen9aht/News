//
//  NewsCollectionViewController.swift
//  News
//
//  Created by MAC12166 on 6/18/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import UIKit
import PKHUD

private let reuseIdentifier = "NewsCell"
private var allowReload: Bool = false
class NewsCollectionViewController: UICollectionViewController {

    var presenter: NewsListPresenterProtocol?
    var newsList: [NewsModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.register(UINib.init(nibName: "NewsCollectionCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            flowLayout.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width, height: 60)
        }

        presenter = NewsListPresenter()
        presenter?.view = self
        
        guard let isLoaded = presenter?.loadView() else {
            return
        }
        
        if !isLoaded {
            HUD.flash(.label("Could not load the view!"), delay: 2.0)
        }
    }
}

extension NewsCollectionViewController: NewsListViewProtocol, UICollectionViewDelegateFlowLayout{
    func showAll(_ newsList: [NewsModel]) {
        if newsList.count == 0 {
            HUD.flash(.label("The file is empty"), delay: 2.0)
            return
        }
        self.newsList = newsList
        collectionView?.reloadData()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.newsList.count
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? NewsCollectionCell else{
            print("Could not dequeue reusablecell!")
            return UICollectionViewCell()
        }
        
        cell.update(self.newsList[indexPath.row])

        // Configure the cell
        allowReload = true
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.showDetail(news: newsList[indexPath.row])
    }
    
    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard
            let previousTraitCollection = previousTraitCollection,
            self.traitCollection.verticalSizeClass != previousTraitCollection.verticalSizeClass ||
                self.traitCollection.horizontalSizeClass != previousTraitCollection.horizontalSizeClass
            else {
                return
        }
        
        self.collectionView?.collectionViewLayout.invalidateLayout()
        self.collectionView?.reloadData()
    }
    
    open override func viewWillTransition(to size: CGSize,
                                          with coordinator: UIViewControllerTransitionCoordinator) {
        if !allowReload{
            return
        }
        super.viewWillTransition(to: size, with: coordinator)
        
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else{
            return
        }
        
       
        flowLayout.invalidateLayout()
        collectionView?.reloadData()
        
    }
}
