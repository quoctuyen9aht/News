//
//  NewsListPresenterProtocol.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation

protocol NewsListPresenterProtocol {
    var interactor: NewsListInteractorProtocol? { get set }
    var view: NewsListViewProtocol? { get set }
    var routing: NewsListRouterProtocol? { get set }
    func loadView() -> Bool
    func showDetail(news: NewsModel)
}
