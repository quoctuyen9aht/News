//
//  NewsListDataManager.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation

class NewsListDataManager:NewsListDataManagerProtocol{
    private let file: String
    init(using nameFile: String) {
        self.file = nameFile
    }
    func retrieveNewsList() -> [News] {
        
        guard let url = Bundle.main.url(forResource: file, withExtension: "json") else{
            return []
        }
        
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode(Array<News>.self, from: data)
            return jsonData
        } catch {
            print("Could not load the json file!")
        }
        
        return []
    }
}
