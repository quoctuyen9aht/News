//
//  NewsListInteractor.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
import SafariServices

class NewsListInteractor: NewsListInteractorProtocol{
    final let fileName = "data"
    var dataManager: NewsListDataManagerProtocol?

    init(){
        dataManager = NewsListDataManager(using: fileName)
    }
    
    func retrieveNewsList() -> [NewsModel]{
        
        guard let newsList = dataManager?.retrieveNewsList() else{
            return [NewsModel]()
        }
        
        return newsList.map({news in
            let newsModel = NewsModel()
            
            newsModel.title = news.title ?? ""
            newsModel.content1 = news.content1 ?? ""
            newsModel.content2 = news.content2 ?? ""
            newsModel.imageUrl = news.imageUrl ?? ""
            newsModel.author = news.author ?? ""
            
            guard let newsTime = news.time else {
                return newsModel
            }
            
            let dateGetter = DateFormatter()
            dateGetter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            
            if let time =  dateGetter.date(from: newsTime) {
                newsModel.time = time
            }
            return newsModel
        });
    }
}
