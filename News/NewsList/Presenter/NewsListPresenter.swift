//
//  NewsListViewPresenter.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
import UIKit

class NewsListPresenter: NewsListPresenterProtocol{
    var interactor: NewsListInteractorProtocol?
    var view: NewsListViewProtocol?
    var routing: NewsListRouterProtocol?
    
    init() {
        self.interactor = NewsListInteractor()
        self.routing = NewsListRouter()
    }
    
    func loadView() -> Bool{
        if let list = interactor?.retrieveNewsList(){
            view?.showAll(list)
            return true
        }
        
        return false
    }
    
    func showDetail(news: NewsModel) {
        if let view = view as? UIViewController{
            routing?.presentNewsDetail(fromView: view, forNews: news)
        }
    }
}
